Introduction
============
This module allows you to override the theme to be used on a per-variant basis.

It also includes an abstraction layer between theme and an arbitrary label, so
that you can easily switch themes for all variants using a specific association
without having to manually modify each one. This is useful for marketing
campaigns where variants need to use a sub-theme temporarily.

Installation
============
Enable the module as usual.

Configuration
=================================================
1. Go to admin/appearance/settings, there is a new section "Theme Associations".
2. Make theme associations by adding a label and selecting a theme.
3. Edit a panel or create a new one, and under variant categories you will see
   a new category "Custom Theme".
4. Enable "Custom Theme" for a variant and choose a theme association you
   created in step #2. Save the variant.
5. Visit a page that uses the variant you just modified, and the theme will be
   the one you've chosen!
